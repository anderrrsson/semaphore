from django.apps import AppConfig


class SemaforoConfig(AppConfig):
    name = 'Semaforo'
