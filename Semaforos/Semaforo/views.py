from django.http import HttpResponse
from django.shortcuts import render
from .functions import semaphore_process
from django.views.decorators.clickjacking import xframe_options_exempt
from datetime import datetime

current_date = datetime.now()

business_hours = current_date.time()


def hi(request):
    return render(request, 'semaphore/hi.html')


@xframe_options_exempt
def personal_semaphore(request, pk):
    if (business_hours.hour >= 8) and (business_hours.hour <= 24):
        semaphore_status_info, ids = semaphore_process(pk)
        if not ids:
            return HttpResponse('<h5>Esta Sale Order aun no ha sido creada</h5>')
        if semaphore_status_info is None:
            return HttpResponse('<h5> Error de Autenticacion </h5>')
        if semaphore_status_info == -1:
            return HttpResponse('<h5>Este seamforo no ha iniciado aun</h5>')
        else:
            return render(request, 'semaphore/main.html', semaphore_status_info)
    else:
        return HttpResponse('<h5>la hora actual esta fuera de horario laboral </h5>')
