from django.urls import path
from . import views

urlpatterns = [
    path('', views.hi, name='home-page'),
    path('<str:pk>', views.personal_semaphore, name='list-page'),
]
