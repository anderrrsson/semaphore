from xmlrpc import client
import json
from datetime import datetime
import os

db_name = os.getenv('ODOO_DATABASE')
username = os.getenv('ODOO_USERNAME')
password = os.getenv('ODOO_PASSWORD')
server_url = os.getenv('URL_ODOO_SERVICE')


def odoo_auth():
    common = client.ServerProxy('%s/xmlrpc/2/common' % server_url)
    user_id = common.authenticate(db_name, username, password, {})
    models = client.ServerProxy('%s/xmlrpc/2/object' % server_url)

    return user_id, models


def semaphore_process(pk):
    user_id, models = odoo_auth()
    if user_id:
        orders_ids = models.execute_kw(db_name, user_id, password, 'sale.order', 'search', [[['name', '=', pk]]])
        orders_data = models.execute_kw(db_name, user_id, password, 'sale.order', 'read',
                                        [orders_ids, ['state', 'quote_level', 'quote_submission_date']])
        y = json.dumps(orders_data)
        sale_order = json.loads(y)
        semaphore_status = ''

        for record in sale_order:
            quote_type = get_quote_type(record)
            time_current = set_delivery_quote_date(record)
            if time_current == -1:
                return -1
            if quote_type == 'general':
                semaphore_status = set_general_semaphore_status(time_current)
            elif quote_type == 'specific':
                semaphore_status = set_specific_semaphore_status(time_current)
            elif quote_type == 'special':
                semaphore_status = set_special_semaphore_status(time_current)
        context = {
            "status": semaphore_status,
        }
        return context, orders_ids
    else:
        return None


def get_quote_type(record):
    for key in record:
        if key == 'quote_level':
            quote_type = record[key]
            return quote_type


def set_delivery_quote_date(record):
    for key in record:
        if key == 'quote_submission_date':
            if record[key]:
                quote_sent_date = datetime.strptime(record[key], '%Y-%m-%d %H:%M:%S')
                hour_difference = quote_sent_date - datetime.now()
                time_current = hour_difference.seconds / 3600
                return time_current
            else:
                return -1


def set_general_semaphore_status(time_current):
    if time_current < 1:
        semaphore_status = 'Green'
        return semaphore_status
    elif (time_current > 1) and (time_current < 3):
        semaphore_status = 'Yellow'
        return semaphore_status
    elif time_current > 3:
        semaphore_status = 'Red'
        return semaphore_status


def set_specific_semaphore_status(time_current):
    if time_current < 5:
        semaphore_status = 'Green'
        return semaphore_status
    elif (time_current > 5) and (time_current < 10):
        semaphore_status = 'Yellow'
        return semaphore_status
    elif time_current > 10:
        semaphore_status = 'Red'
        return semaphore_status


def set_special_semaphore_status(time_current):
    if time_current < 12:
        semaphore_status = 'Green'
        return semaphore_status
    elif (time_current > 12) and (time_current < 24):
        semaphore_status = 'Yellow'
        return semaphore_status
    elif time_current > 24:
        semaphore_status = 'Red'
        return semaphore_status
